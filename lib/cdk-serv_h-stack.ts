import * as cdk from 'aws-cdk-lib';
import * as lambda from 'aws-cdk-lib/aws-lambda';
import * as apigw from 'aws-cdk-lib/aws-apigateway';

export class CdkServHStack extends cdk.Stack {
  constructor(scope: cdk.App, id: string, props?: cdk.StackProps) {
    super(scope, id, props);


    // Function that returns 201 with "Hello world!"
    const helloWorldFunction = new lambda.Function(this, 'helloWorldFunction', {
      code: new lambda.AssetCode('src'),
      handler: 'src/handler.handler',
      runtime: lambda.Runtime.NODEJS_14_X
    });

    // Rest API backed by the helloWorldFunction
    const helloWorldLambdaRestApi = new apigw.LambdaRestApi(this, 'helloWorldLambdaRestApi', {
      restApiName: 'Hello World API',
      handler: helloWorldFunction,
      proxy: false,
    });

    // Hello Resource API for the REST API. 
    const hello = helloWorldLambdaRestApi.root.addResource('HELLO');

    // GET method for the HELLO API resource.
    hello.addMethod('GET', new apigw.LambdaIntegration(helloWorldFunction))
  }
}

